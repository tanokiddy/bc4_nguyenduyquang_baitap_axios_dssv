const BASE_URL = "https://62db6ca2d1d97b9e0c4f3362.mockapi.io";
function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      tatLoading();
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
getDSSV();

function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}
function themSV() {
  //lay data object tu mock
  var newSv = {
    name: document.querySelector("#txtTenSV").value,
    email: document.querySelector("#txtEmail").value,
    id: document.querySelector("#txtMaSV").value * 1,
  };
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSv,
  })
    .then(function (res) {
      console.log(res);
      tatLoading();
      getDSSV();
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function suaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      showThongTinTuForm(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}

function resetSV() {
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtMaSV").value = "";
}

function searchSV() {
  var id = document.querySelector("#txtSearch").value;
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      renderSV(res.data);
    })
    .catch(function (err) {
      console.log(err);
      renderSVErr(err.data);
    });
}

function updateSV() {
  var id = document.querySelector("#txtMaSV").value;
  var updateSV = {
    name: document.querySelector("#txtTenSV").value,
    email: document.querySelector("#txtEmail").value,
  };
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "PUT",
    data: updateSV,
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
      renderDSSV(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}
