renderDSSV = function (dssv) {
  var contentHTML = "";

  dssv.forEach((sv) => {
    var contentTr = `
        <tr>
          <td>${sv.id}</td>
          <td>${sv.name}</td>
          <td>${sv.email}</td>
          <td>0<td>
          <button onclick=xoaSinhVien('${sv.id}') class="btn btn-danger">Xoá</button>
          <button onclick=suaSinhVien('${sv.id}') class="btn btn-warning">Sửa</button>
        </tr>
      `;
    contentHTML += contentTr;
  });
  console.log("contentHTML: ", contentHTML);
  document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
};

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
function showThongTinTuForm(sv) {
  document.getElementById("txtTenSV").value = sv.name;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtMaSV").value = sv.id;
}

function renderSV(dssv) {
  var contentHTML = "";

  var dssv = `
        <tr>
          <td>${dssv.id}</td>
          <td>${dssv.name}</td>
          <td>${dssv.email}</td>
          <td>0<td>
          <button onclick=xoaSinhVien('${dssv.id}') class="btn btn-danger">Xoá</button>
          <button onclick=suaSinhVien('${dssv.id}') class="btn btn-warning">Sửa</button>
        </tr>
      `;
  contentHTML += dssv;

  console.log("contentHTML: ", contentHTML);
  document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
}
function renderSVErr(dssv) {
  var contentHTML = "";

  var dssv = `
        <tr>
          <td>Không tìm thấy</td>
        </tr>
      `;
  contentHTML += dssv;

  console.log("contentHTML: ", contentHTML);
  document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
}
